function webDevelopers(data){
    if(data === undefined || typeof data !== 'object')
        return [];

    else{
        return data.filter(person => person['job'].indexOf('Web Developer') !== -1);
    }
}

function properNumber(data){

    let newSalary = [];

    if(data === undefined || typeof data != 'object')
        return [];
    else
        data.forEach(person => {
            person['salary'] = parseFloat(person['salary'].split('$')[1]);
            newSalary.push(person);
        });

    return newSalary;

}

function sumOfSalarys(data){

    if(data === undefined || typeof data !== 'object')
        return null;
    else{
        return data.map(person => person['salary'] = parseFloat(person['salary'].split('$')[1])).reduce((acc,curr) => acc += curr);
    }

}